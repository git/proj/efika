# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-kernel/gentoo-sources/gentoo-sources-2.6.19-r1.ebuild,v 1.2 2006/12/02 09:40:33 corsair Exp $

ETYPE="sources"
K_WANT_GENPATCHES="base extras"
K_GENPATCHES_VER="2"
inherit kernel-2
detect_version
detect_arch

EFIKA_PATCHES="efika-patches-2.6.19-r2.tar.bz2"
#EFIKA_PATCHES="linux-2.6.19-rc6_patches.tar.gz"

UNIPATCH_LIST="${DISTDIR}/${EFIKA_PATCHES}"

KEYWORDS="~amd64 ~ppc ~ppc64 ~sparc ~x86"
HOMEPAGE="http://dev.gentoo.org/~dsd/genpatches"

DESCRIPTION="Full sources including the Gentoo patchset for the ${KV_MAJOR}.${KV_MINOR} kernel tree"
SRC_URI="${KERNEL_URI} ${GENPATCHES_URI} ${ARCH_URI}
	http://dev.gentoo.org/~humpback/efika/${EFIKA_PATCHES}"

#src_unpack() {
#	kernel-2_src_unpack
#	die "unpacked"
#}

pkg_postinst() {
	kernel-2_pkg_postinst
	einfo "For more info on this patchset, and how to report problems, see:"
	einfo "${HOMEPAGE}"
}
